.. Haliotis documentation master file, created by
   sphinx-quickstart on Sun Nov  4 11:17:25 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Haliotis Documentation
======================
Welcome to Haliotis's documentation!
In here you will find instructions on how to install, a quick-start that shows how to build a simple engine, as well as detailed API documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/installation
   usage/quickstart
   usage/board2d
   usage/aep



Indices and tables:

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
