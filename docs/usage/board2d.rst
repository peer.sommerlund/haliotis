Module Board2D
==============

The Board2D module provides a simple 2D array-of-arrays representation of a board.


Coordinate system
-----------------

The board is a hexagonal area where coordinates are named (q,r). In the diagram below (0,0) is the upper left corner, and invalid positions are marked 'x'::

    x,x,x,x, 0,0,0,0,0,
     x,x,x, 0,0,0,0,0,0,
      x,x, 0,0,0,0,0,0,0,
       x, 0,0,0,0,0,0,0,0,
         0,0,0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,0, x,
           0,0,0,0,0,0,0, x,x,
            0,0,0,0,0,0, x,x,x,
             0,0,0,0,0, x,x,x,x,

The directions can be visualized like this::

      4 5
     3 * 0
      2 1

Classes
-------
The module has four classes. 

* :class:`haliotis.Dir`
* :class:`haliotis.Pos`
* :class:`haliotis.Move`
* :class:`haliotis.Board`

.. autoclass:: haliotis.Dir
	:members:
	:undoc-members:

.. autoclass:: haliotis.Pos
	:inherited-members:

.. autoclass:: haliotis.Move
	:inherited-members:

To construct a board, you provide a  BoardGrid - which is an list of lists of fields. The following are defined for you:

* haliotis.BelgianDaisy
* haliotis.DutchDaisy
* haliotis.GermainDaisy
* haliotis.SwissDaisy

.. autoclass:: haliotis.Board
	:inherited-members:
