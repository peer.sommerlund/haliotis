Module AEP
==========


   
.. automodule:: haliotis.aep
   

Your engine must be implemented as a class that realizes the :class:`Engine` interface. To launch the engine, use :meth:`play()`

.. autoclass:: haliotis.aep.Engine
    :members:

You also need to provide proper replies to commands. This is taken care of for most replies, but the 'info' reply you have to implement yourself. 

To start your engine you create an instance and send it to the function :meth:`play()`

.. autofunction:: haliotis.aep.play
