Quick Start
===========
Haliotis provides an implementation of the rules for the board game Abalone. It can be used to build computer engines, user interfaces, endgame analyzers, etc.

First, you need to have an Abalone board you can play on. If you want to start from Belgian Daisy write like this

.. code-block:: python

    from haliotis import *
    board = Board()
    board.reset_to(BelgiumDaisy)


To iterate over all moves two ply

.. code-block:: python

    for m1 in board.all_moves():
        board.do_move(m1)
        for m2 in board.all_movces():
            board.do_move(m2)
            print('moves {} {}'.format(m1, m2))
            print('{}'.format(as_str(board)))
            board.undo_move(m2)
        board.undo_move(m1)


A Simple Engine
---------------
An engine is no fun unless you can play it. The easiest way to do that is to implement the standard Abalone Engine Protocol. This is similar to how UCI chess engines work, in that all communication with the engine is done over stdio. Once you have this protocol implemented, you can use the [Blackbird server|http://google.com] to play with your engine, or to arrange test tournaments between different versions of the engine.

The AEP protocol is documented here <insert link> if you want to implement it yourself. 

Haliotis provides a wrapper function :meth:`haliotis.aep.play` that handles parsing of commands and generation of most replies, except a few. To use this function, you have to create a specialisation of :class:`haliotis.aep.Engine`, like the following example.

.. code-block:: python

    class MyEngine(haliotis.aep.Engine):
        def __init__(self):
            super().__init__()
        def get_name(self):
            return 'My amazing engine'
        def get_author(self):
            return 'My name'
        def set_game(self, game):
            '''Set up board position and moves leading to it'''
            self.board = game.current_board()
        def stop_search(self):
            '''Tell engine to set a flag so it will stop 
            searching.'''
            self.abort_search = True
        def get_move(self):
            '''Get move from engine (ask it do perform a search)'''
            self.abort_search = False
            # My amazing algorithm for finding the best move
            for _ in range(1000):
                # .. do magic search logic ..

                # Ask haliotis.Engine to check for AEP commands 
                # while we search, eg. every 100 ms
                self.check_input()

                # Note that one AEP command may be to stop 
                # searching now. That will trigger
                # haliotis.Engine.stop_search to be called, 
                # which we implemented above.
                if self.abort_search:
                    break

            # Return best move found so far
            return random.choice(board.all_moves())

Once you have your engine object, you can launch it from the wrapper.

.. code-block:: python

    engine = MyEngine()
    haliotis.aep.play(engine)
