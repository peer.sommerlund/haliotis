import setuptools
import re

with open("README.md", "r") as f:
    long_description = f.read()

with open('haliotis/__init__.py', 'rt', encoding='utf8') as f:
    version = re.search(r'__version__ = \'(.*?)\'', f.read()).group(1)

setuptools.setup(
    name="haliotis",
    version=version,
    author="Peer Sommerlund",
    author_email="peso@users.sourceforge.net",
    description="Library for playing the board game Abalone",
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires=">=3.5",
    packages=setuptools.find_packages(),
    classifiers=[
        "Topic :: Games/Entertainment :: Board Games",
        "Intended Audience :: Developers",
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v2 or later (LGPLv2+)",
        "Operating System :: OS Independent",
    ],
)
