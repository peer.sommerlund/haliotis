# PrintGame lists all positions in an abalone game

# Python packages
import sys
import time

# Third party packages
import haliotis


def print_all(game_file):
    game = haliotis.LinearGame()
    with open(game_file) as fi:
        haliotis.persistence.agf_read(game, fi)
    if 'Black' in game.tag_keys and 'White' in game.tag_keys:
        print(f'Black: {game.get_tag("Black")}')
        print(f'White: {game.get_tag("White")}')
    game.undo_all_moves()
    print(game.current_board())
    while game.more_moves_to_redo():
        move_str = haliotis.as_ab_move(game.current_board(), game.next_move())
        print(f'Move {game.current_board_number()} : {move_str}')
        game.redo_move()
        print(game.current_board())
    print('End of game')

if __name__ == "__main__":
    if len(sys.argv) < 1+1:
        print('This program prints all positions in the main line of a game')
        print('Missing game.ag file')
        exit(1) 
    print_all(sys.argv[1])