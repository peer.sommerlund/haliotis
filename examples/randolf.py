# Randolf is an Abalone engine. It performs random moves.
# The engine interface is AEP via stdio

# Python packages
import logging
import random

# Third party packages
import haliotis


#
#  Logging
#

logging.basicConfig(
    level=logging.DEBUG,
    style='{',
    format='{asctime} {module} {levelname} {message}',
    filename='randolf.log')
log = logging.getLogger('aep')
# Console handler with less data
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(
    logging.Formatter(
        '%(levelname)-8s %(message)s'

))
log.addHandler(ch)
log = logging.getLogger('randolf')


#
#  The engine
#

class Randolf(haliotis.aep.Engine):
    def __init__(self):
        super().__init__()
    def get_name(self):
        return 'Randolf'
    def get_author(self):
        return 'Peer Sommerlund'
    def set_game(self, game):
        '''Set up board position and moves leading to it'''
        log.debug('set up board position')
        self.board = game.current_board()
    def stop_search(self):
        '''Tell engine to set a flag so it will stop
        searching.'''
        self.abort_search = True
    def get_move(self, **kwargs):
        '''Get move from engine (ask it do perform a search)'''
        log.debug('Begin search for best move')
        self.abort_search = False

        # Timed search - wait until times up
        # Fixed nodes search - do that number of random move generations, then move
        # other special parameters - print and ignore
        for _ in range(1000):
            # .. do magic search logic ..

            # Ask haliotis.Engine to check for AEP commands
            # while we search, eg. every 100 ms
            self.check_input()

            # Note that one AEP command may be to stop
            # searching now. That will trigger
            # haliotis.Engine.stop_search to be called,
            # which we implemented above.
            if self.abort_search:
                break

        # Return best move found so far
        best_move = random.choice(list(self.board.moves()))
        log.debug('Best move found is: {}'.format(best_move))
        return best_move

engine = Randolf()
haliotis.aep.play(engine)