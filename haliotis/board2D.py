#
#   Haliotis, a library for Abalone playing programs.
#
#   :license: LGPL, see LICENSE for details.
#   :copyright: 2019 Peer Sommerlund
#
"""
    This module defines the Board2D class which holds an abalone board.
"""

from enum import IntEnum
from typing import Iterator

from . import HaliotisException

# ---- Direction ----------------------------------------------

class Dir(IntEnum):
    """ A Hexagonal direction. There are six different directions """
    # Range: -1..5
    # Illegal, E, S, SW, W, N, NE
    Invalid = -1
    Right = 0
    RightDown = 1
    LeftDown = 2
    Left = 3
    LeftUp = 4
    RightUp = 5
    #  4 5
    # 3 * 0
    #  2 1
    
    def opposite(self):
        '''180 degree direction'''
        return Dir((self.value + 3) % 6)

    def left(self):
        '''one step anti-clockwise'''
        return Dir((self.value - 1 +6) % 6)

    def right(self):
        '''one step clockwise'''
        return Dir((self.value + 1) % 6)





























# ---- BoardPos -----------------------------------------------


class Pos:
    ''' A position on the board. Since the board is 9x9 it is possible
    to have invalid coordinates. This class can validate itself and
    generate the next successive move. 
    
    (q,r) upper left corner is (0,0)
    q grows to the right
    r grows down-right
    '''
    '''
    x,x,x,x, 0,0,0,0,0,
     x,x,x, 0,0,0,0,0,0,
      x,x, 0,0,0,0,0,0,0,
       x, 0,0,0,0,0,0,0,0,
         0,0,0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,0, x,
           0,0,0,0,0,0,0, x,x,
            0,0,0,0,0,0, x,x,x,
             0,0,0,0,0, x,x,x,x,
    '''
    def __init__(self, *argv):
        if len(argv) == 0:
            self.x = -1
            self.y = -1
        elif len(argv) == 1:
            p = argv[0]
            self.x = p.x
            self.y = p.y
        elif len(argv) == 2:
            ax, ay = argv
            self.x = ax
            self.y = ay
        else:
            raise Exception("Invalid number of parameters")
    
    def __repr__(self):
        return 'Pos({},{})'.format(self.x, self.y)
    
    def __str__(self):
        from .persistence import as_ab_pos
        return as_ab_pos(self)
    
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    
    def is_valid(self):
        ''' True, if the position can contain a piece. Since we have a
        hexagonal board and represent it in a square (an array) not all of the
        positions are legal. '''
        return ((0<=self.x) and (self.x<=8) and (0<=self.y) and (self.y<=8)
          and (4+0<=self.x+self.y) and (self.x+self.y<=4+8))

    def neighbour(self, dir):
        ''' Return the Board2D::Pos one step in any of the six directions.
        No check is performed for if the new position is valid. '''
        p = Pos(self)
        p.step(dir)
        return p

    def step(self, dir):
        '''Move this position one step in any of the six directions.
        No check is performed for if the new position is valid. '''
        
        if dir == Dir.Right:
            self.x += 1
        elif dir == Dir.RightDown:
            self.y += 1
        elif dir == Dir.LeftDown:
            self.x -= 1
            self.y += 1
        elif dir == Dir.Left:
            self.x -= 1
        elif dir == Dir.LeftUp:
            self.y -= 1
        elif dir == Dir.RightUp:
            self.x += 1
            self.y -= 1
        else:
            raise ValueError("Direction out of range")

    def direction_to(self, p):
        ''' Return direction to another Board2D.Pos, but only if on one of
        the six axes relative to this Board2D.Pos '''
        assert isinstance(p, Pos)
        result = Dir.Invalid
        dx=p.x-self.x; dy=p.y-self.y

        if   (dy==0)   : result=Dir.Right; delta=dx
        elif (dx==0)   : result=Dir.RightDown; delta=dy
        elif (dx==-dy) : result=Dir.LeftDown; delta=dy
        else           : return Dir.Invalid

        if (delta<0) : result = Dir(result+3)
        return result

def dist(a: Pos, b: Pos) -> int:
    '''Distance along an axis, if not axisaligned, return -1'''
    dx = b.x - a.x
    dy = b.y - a.y
    if dx == 0: return abs(dy)
    if dy == 0: return abs(dx)
    if dx+dy == 0: return abs(dx)
    return -1

def line_length(a: Pos, b: Pos):
    '''Number of marbles in the provided line, endpoints included'''
    if not a.is_valid() or not b.is_valid(): return -1
    return dist(a,b)+1

def col_range(row):
    '''Iterate columns in a given row'''
    return range(max(0,4-row), min(9, 9+4-row))
    
def all_pos():
    '''Iterate valid positions in reading order: Top-bottom, then left-right'''
    for r in range(9):
        for c in col_range(r):
            yield Pos(c,r)

# ---- Move ---------------------------------------------------

class Move:
    ''' Move is a transition from one Board to another.
    Think of it as a Command pattern to a Board.
    
    * Move() creates an invalid move
    * Move(ff,tf) creates a move of a single piece
    * Move(ff,fl,tf) creates a multi-piece move, inline or broadside.
    '''
    def __init__(self, *argv):
        if len(argv) == 0:
            self.clear()
        elif len(argv) == 2:
            # Move single piece (ff,tf)
            assert isinstance(argv[0],Pos)
            assert isinstance(argv[1],Pos)
            self.head = argv[0]
            self.tailDir = self.head.direction_to(argv[1])
            self.tailCount = 1
            self.pushCount = 0
            self.moveDir = self.tailDir
        elif len(argv) == 3:
            #  Move several pieces
            from_first, from_last, to_first = argv
            assert isinstance(from_first, Pos)
            assert isinstance(from_last, Pos)
            assert isinstance(to_first, Pos)
            self.head = from_first
            self.tailDir = self.head.direction_to(from_last)
            self.tailCount = line_length(from_first, from_last)
            self.pushCount = 0
            self.moveDir = self.head.direction_to(to_first)
        else:
            raise Exception("Invalid numberof parameters")

    def clear(self):
        self.head = Pos()
        self.tailDir = Dir.Right
        self.tailCount = 1
        self.pushCount = 0
        self.moveDir = Dir.Right

    def __eq__(self, other): 
        return self.__dict__ == other.__dict__
    def __repr__(self):
        return self.__str__()
    def __str__(self):
        if self.tailDir == self.moveDir:
            return '{}p{}{}{}'.format(
                self.head,
                int(self.tailDir), self.tailCount, self.pushCount
            )
        elif self.tailDir.left() == self.moveDir:
            return '{}L{}{}'.format(
                self.head,
                int(self.tailDir), self.tailCount
            )
        elif self.tailDir.right() == self.moveDir:
            return '{}R{}{}'.format(
                self.head,
                int(self.tailDir), self.tailCount
            )
        else:
            return '{}t{}{}{}m{}'.format(
                self.head,
                int(self.tailDir), self.tailCount, self.pushCount,
                int(self.moveDir)
            )

    @property
    def from_first(self):
        '''Start position of first piece to move.'''
        return Pos(self.head)
        
    @property
    def from_last(self):
        '''Start position of last piece to move.'''
        result = Pos(self.head)
        if self.tailCount >= 2: result.step(self.tailDir)
        if self.tailCount >= 3: result.step(self.tailDir)
        return result
        
    @property
    def from_middle(self):
        '''Start position of middle piece to move.'''
        result = Pos(self.head)
        for i in range(2,self.tailCount):
            result.step(self.tailDir)
        return result

    @property
    def to_first(self):
        '''End position of first piece to move'''
        return Pos(self.from_first).neighbour(self.moveDir)

    @property
    def to_last(self):
        '''End position of last piece to move'''
        return Pos(self.from_last).neighbour(self.moveDir)

    @property
    def to_middle(self):
        '''End position of middle piece to move'''
        return Pos(self.from_middle).neighbour(self.moveDir)
        
    def is_valid(self):
        ''' is move inside board? To determine if a move is truly valid,
        you have to do it on a Board.'''
        if self.moveDir == Dir.Invalid: return False
        if self.tailDir == Dir.Invalid: return False
        result = self.from_first.is_valid() and self.to_first.is_valid()
        result = result and 0 < self.tailCount and self.tailCount <= 3
        if self.tailCount > 1:
            result = result and self.from_last.is_valid() and self.to_last.is_valid()
        result = result and self.pushCount < self.tailCount
        return result

# ---- Board --------------------------------------------------

from typing import List
BoardGrid = List[List[int]]

class InvalidMove(HaliotisException):
    def __init__(self, error_msg):
        self.error_msg = error_msg
    def __str__(self):
        return self.error_msg

class Board:
    '''A Board knows the position of all pieces, which player has the turn, and the score for each player. It can generate valid moves, it can return what is at a given position.
    Internally, it uses a 9x9 representation so the corners are not used.
    '''
    def __init__(self, board: BoardGrid = None):
        self.players = 2
        if board:
            self.set_up(board)
        else:
            self.clear()
        
    def __copy__(self):
        a = Board()
        a.player_to_move = self.player_to_move
        a.field = [[self.field[r][c] for c in range(9)] for r in range(9)]
        a.player_out = [po for po in self.player_out]
        a.currentHashCode = 0
        return a
    
    def copy(self):
        '''Return a duplicate of this board'''
        return self.__copy__()
    
    def clear(self):
        '''Clears the board and number of pieces pushed out'''
        self.player_to_move = 1
        self.field = [[0]*9 for _ in range(9)]
        self.player_out = [0] * (self.players+1)
        self.currentHashCode = 0
    
    def __eq__(self, other):
        return self.__repr__() == other.__repr__()
    
    def __repr__(self):
        return self.as_str_naked()
    
    def __str__(self):
        return self.as_str_decorated()
    
    def as_str_decorated(self):
        '''ASCII art of the board, including labels on axises for official
        Abalone Position coordinates. Use this for pretty-printing in log
        files.'''
        rows = 'ihgfedcba'
        cols = '123456789'
        lines = self.as_str_naked().split('\n')
        for r in range(0,9):
            row_id = '{} - '.format(rows[r])
            x = max(4-r, r-4)
            lines[r] = lines[r][:x] + row_id + lines[r][x:]
        lines[9:9] = [
            r'         '+' '.join(5*['\\']),
            r'          '+' '.join(list(cols[0:5]))
        ]
        for r in range(5,9):
            lines[4+9-r] += ' \\'
        for r in range(5,9):
            lines[5+9-r] += ' '+cols[r]
        return '\n'.join(lines)#.replace('\\',' ').replace('-',' ')
    
    def as_str_naked(self):
        '''The board as 9 line ascii art + 1 line showing score and side to
        move. If all blanks and newlines are removed, this is a valid
        Abalone Board Position format string'''
        def str_at(r,c):
            f = self.field[r][c]
            return '.' if f==0 else str(f)
        res = []
        for r in range(0,9):
            res.append(' ' * max(4 - r, r - 4))
            res.append(' '.join([
                str_at(r,c) for c in col_range(r)
            ]))
            res.append('\n')
        res.append('{} '.format(self.player_to_move))
        for p in range(1, self.players+1):
            opponent = 3 - p
            score = self.player_out[opponent]
            res.append(' {}'.format(score))
        return ''.join(res)
    
    @staticmethod    
    def parse(abp_str: str):
        '''Create a Board instance from a string representation
        abp_str: a string in Abalone Board Position format
        '''
        self = Board()
        abp_str = abp_str.replace('.','0').replace(' ','').replace('\n','')
        v = [int(f) for f in abp_str if f in '0123456']
        if len(v) != 61 + 3:
            raise ValueError('Expected 64 fields in Board string, found {}'.format(len(v)))
        i = 0
        for r in range(0,9):
            for c in col_range(r):
                if v[i] > 2:
                    raise ValueError('Board positions with more than two players is not supported')
                self.set_board_pos(Pos(c,r), v[i])
                i+=1
        self.player_to_move = v[i]; i+=1
        
        # string contains score, but Board counts loss.
        self.player_out[2] = v[i]; i+=1 # Score 1 = Loss 2
        self.player_out[1] = v[i]; i+=1 # Score 2 = Loss 1
        
        # player 0 counts total marbles out
        self.player_out[0] = - self.player_out[1] - self.player_out[2]
        
        return self

    def verify_move(self, m: Move):
        ''' Given a move, see if it is valid on this board.
        Raises an exception if not.
        '''
        # Verify move
        assert m.is_valid()
        if (self.at(m.from_first) != self.turn()
        or self.at(m.from_middle) != self.turn()
        or self.at(m.from_last) != self.turn()):
            print(self)
            print(m)
            print('ff,fm,fl = {},{},{} -> {},{},{}'.format(m.from_first, m.from_middle, m.from_last
                ,self.at(m.from_first)
                ,self.at(m.from_middle)
                ,self.at(m.from_last)
                ))
            # tried to move nothing, or enemy
            raise InvalidMove('Can only move own pieces')
        if dist(m.from_first, m.from_last) > 2:
            raise InvalidMove('Agressor too long')
        if m.pushCount >= m.tailCount:
            raise InvalidMove('Agressor too short')
        # Check opponent pieces pushed
        push_to_last = m.to_last
        opponent = 3 - self.turn()
        for _ in range(m.pushCount):
            assert push_to_last.is_valid()
            if self.at(push_to_last) != opponent:
                raise InvalidMove('Wrong push count')
            push_to_last.step(m.tailDir)
        if push_to_last.is_valid() and self.at(push_to_last) != 0:
            raise InvalidMove('Need space to push to')
    
    def do_move(self, m: Move):
        ''' Given a move, this function tries to move the pieces on the board.

        If the move is invalid, will raise an exception (see verify_move)
        '''
        self.verify_move(m)
        
        # Variables
        opponent = 3 - self.turn()
        push_to_last = m.to_last
        for _ in range(m.pushCount):
            push_to_last.step(m.tailDir)
        
        # Perform move
        if m.moveDir == m.tailDir:
            # inline move
            self.set_board_pos(m.from_first,0)
            self.set_board_pos(m.to_last, self.turn())
            if m.pushCount > 0 and push_to_last.is_valid():
                self.set_board_pos(push_to_last, opponent)
        else:
            # broadside move
            p = Pos(m.from_first)
            for _ in range(m.tailCount):
                self.set_board_pos(p,0)
                self.set_board_pos(p.neighbour(m.moveDir), self.turn())
                p.step(m.tailDir)
        self.end_turn()
    
    def score(self, player: int):
        '''Get number of marbles pushed out by this player.
        Win when score reaches 6'''
        # NOTE: This only works for 2 player games
        opponent = 3 - player
        return self.player_out[opponent]
    
    def turn(self):
        '''The player to move next'''
        return self.player_to_move
    
    def end_turn(self):
        '''Let the next player have the turn to move'''
        self.player_to_move += 1
        if self.player_to_move > self.players:
            self.player_to_move = 1

    def inc_out(self, player: int, count :int):
        self.player_out[player] += count

    def at(self, bp: Pos) -> int:
        '''Given a position, find out what is there'''
        return self.field[bp.y][bp.x]
        
    def set_board_pos(self, bp: Pos, fv: int):
        '''Set the content of a given position. Updates score for players as needed.'''
        # Count previous content as off board
        self.inc_out(self.field[bp.y][bp.x], 1)
        # Count new content as on board
        self.field[bp.y][bp.x] = fv
        self.inc_out(self.field[bp.y][bp.x],-1)

    def set_up(self, grid: BoardGrid):
        '''Set up the board to the provided BoardGrid, and clear scores of both sides.'''
        self.clear()
        for y in range(9):
            for x in col_range(y):
                self.set_board_pos(Pos(x,y), grid[y][x])
        self.player_out = [0] * (self.players+1)
    
    def moves(self) -> Iterator[Move]:
        '''Iterator over valid moves'''
        for p in all_pos():
            if self.at(p) != self.turn(): continue
            for d in range(6):
                tail_dir = Dir(d)
                pf = p.neighbour(tail_dir)
                if not pf.is_valid(): continue
                # Inline move
                tail_count = 1
                t = Pos(pf)
                while t.is_valid() and self.at(t) == self.turn():
                    tail_count += 1
                    t.step(tail_dir)
                opponent = 3 - self.player_to_move; push_count = 0
                while t.is_valid() and self.at(t) == opponent:
                    push_count += 1
                    t.step(tail_dir)
                if t.is_valid() and self.at(t) == self.turn():
                    push_count = 999 # cannot push opponent and yourself
                if not t.is_valid() and push_count == 0:
                    push_count = 999 # suicide not allowed
                if tail_count <= 3 and push_count < tail_count:
                    m = Move(p,pf)
                    m.tailCount = tail_count
                    m.pushCount = push_count
                    yield m

                # broadside
                if self.at(pf) != self.turn(): continue
                # broadside left
                pl = p.neighbour(tail_dir.left())
                pfl = pf.neighbour(tail_dir.left())
                if pl.is_valid() and self.at(pl) == 0 and pfl.is_valid() and self.at(pfl) == 0:
                    yield Move(p, pf, pl)
                    pff = pf.neighbour(tail_dir)
                    pffl = pff.neighbour(tail_dir.left())
                    if pff.is_valid() and self.at(pff) == self.turn() and pffl.is_valid() and self.at(pffl) == 0:
                        yield Move(p, pff, pl)
                        
                # broadside right
                pr = p.neighbour(tail_dir.right())
                pfr = pf.neighbour(tail_dir.right())
                if pr.is_valid() and self.at(pr) == 0 and pfr.is_valid() and self.at(pfr) == 0:
                    yield Move(p, pf, pr)
                    pff = pf.neighbour(tail_dir)
                    pffr = pff.neighbour(tail_dir.right())
                    if pff.is_valid() and self.at(pff) == self.turn() and pffr.is_valid() and self.at(pffr) == 0:
                        yield Move(p, pff, pr)

# BoardGrid for Board.set_up
GermanDaisy = [
  [0, 0, 0, 0,   0, 0, 0, 0, 0],
   [0, 0, 0,   2, 2, 0, 0, 1, 1],
    [0, 0,   2, 2, 2, 0, 1, 1, 1],
     [0,   0, 2, 2, 0, 0, 1, 1, 0],
      [  0, 0, 0, 0, 0, 0, 0, 0, 0],
       [  0, 1, 1, 0, 0, 2, 2, 0,   0],
        [  1, 1, 1, 0, 2, 2, 2,   0, 0],
         [  1, 1, 0, 0, 2, 2,   0, 0, 0],
          [  0, 0, 0, 0, 0,   0, 0, 0, 0]
]


BelgianDaisy = [
  [0, 0, 0, 0,   2, 2, 0, 1, 1],
   [0, 0, 0,   2, 2, 2, 1, 1, 1],
    [0, 0,   0, 2, 2, 0, 1, 1, 0],
     [0,   0, 0, 0, 0, 0, 0, 0, 0],
      [  0, 0, 0, 0, 0, 0, 0, 0, 0],
       [  0, 0, 0, 0, 0, 0, 0, 0,   0],
        [  0, 1, 1, 0, 2, 2, 0,   0, 0],
         [  1, 1, 1, 2, 2, 2,   0, 0, 0],
          [  1, 1, 0, 2, 2,   0, 0, 0, 0]
]

SwissDaisy = [
  [0, 0, 0, 0,   0, 0, 0, 0, 0],
   [0, 0, 0,   2, 2, 0, 0, 1, 1],
    [0, 0,   2, 1, 2, 0, 1, 2, 1],
     [0,   0, 2, 2, 0, 0, 1, 1, 0],
      [  0, 0, 0, 0, 0, 0, 0, 0, 0],
       [  0, 1, 1, 0, 0, 2, 2, 0,   0],
        [  1, 2, 1, 0, 2, 1, 2,   0, 0],
         [  1, 1, 0, 0, 2, 2,   0, 0, 0],
          [  0, 0, 0, 0, 0,   0, 0, 0, 0]
]


DutchDaisy = [
  [0, 0, 0, 0,   2, 2, 0, 1, 1],
   [0, 0, 0,   2, 1, 2, 1, 2, 1],
    [0, 0,   0, 2, 2, 0, 1, 1, 0],
     [0,   0, 0, 0, 0, 0, 0, 0, 0],
      [  0, 0, 0, 0, 0, 0, 0, 0, 0],
       [  0, 0, 0, 0, 0, 0, 0, 0,   0],
        [  0, 1, 1, 0, 2, 2, 0,   0, 0],
         [  1, 2, 1, 2, 1, 2,   0, 0, 0],
          [  1, 1, 0, 2, 2,   0, 0, 0, 0]
]

TheWall = [
  [0, 0, 0, 0,   0, 0, 2, 0, 0],
   [0, 0, 0,   0, 0, 0, 0, 0, 0],
    [0, 0,   0, 2, 2, 2, 2, 2, 0],
     [0,   2, 2, 2, 2, 2, 2, 2, 2],
      [  0, 0, 0, 0, 0, 0, 0, 0, 0],
       [  1, 1, 1, 1, 1, 1, 1, 1,   0],
        [  0, 1, 1, 1, 1, 1, 0,   0, 0],
         [  0, 0, 0, 0, 0, 0,   0, 0, 0],
          [  0, 0, 1, 0, 0,   0, 0, 0, 0]
]
