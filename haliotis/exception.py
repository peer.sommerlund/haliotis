#
#   Haliotis, a library for Abalone playing programs.
#
#   :license: LGPL, see LICENSE for details.
#   :copyright: 2019 Peer Sommerlund
#
"""
    This module defines the exceptions used by Haliotis
"""

class HaliotisException(Exception):
    pass
