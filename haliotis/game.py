#
#   Haliotis, a library for Abalone playing programs.
#
#   :license: LGPL, see LICENSE for details.
#   :copyright: 2019 Peer Sommerlund
#
"""
    Abalone Game tree
    
    The game tree can hold comments, event info, and variations of moves
"""

from abc import ABC, abstractmethod
from typing import List, Optional

from . import Board, Move
from . import HaliotisException

#
#  Game interface
#

class Game(ABC):
    def __init__(self):
        self.clear_tags()
    
    @abstractmethod
    def restart_from(self, board: Board):
        '''Clear all data, and set up a new start position.'''
        raise NotImplementedError()
    @abstractmethod
    def do_move(self, move: Move):
        '''
          Do a move on the game board. This function also maintains the move tree,
          and automatically expands the tree if a new variant is started in the
          middle of a game. The tree can hold several variants.
          @param move  move to do at the current board
          @pre move  must be a valid normalised move
        Raises an exception if move was not a valid move
        '''
        raise NotImplementedError()
    def redo_move(self, move: Move = None) -> int:
        '''
          Redo a move on the game board. Only moves that are already present in
          the move tree are valid.
          @param move  move to do at the current board
            if not specified, redo the main line
          @pre move  must be a valid normalised move in the game tree
          @return 0 on success, 
                  1 if the move was not found in the move tree at the current node.
                  2 if the move was invalid

        '''
        raise NotImplementedError()
    def undo_move(self):
        raise NotImplementedError()
    def undo_all_moves(self):
        raise NotImplementedError()
    def more_moves_to_undo(self) -> bool:
        '''Are we at the root'''
        raise NotImplementedError()
    def more_moves_to_redo(self) -> bool:
        '''From current position, have we reached the end of the primary variation?'''
        raise NotImplementedError()
    def prev_move(self) -> Optional[Move]:
        raise NotImplementedError()
    def next_move(self) -> Optional[Move]:
        raise NotImplementedError()
    def alternate_moves(self) -> List[Move]:
        '''
          Return all registered moves at the current board.
            @note To do one of these moves, use DoMove.
        '''
        raise NotImplementedError()
    @abstractmethod
    def current_board(self) -> Board:
        raise NotImplementedError()
    def current_board_number(self) -> int:
        '''Compute number of moves used to reach the current position from start_board.'''
        raise NotImplementedError()
    def current_moves(self) -> List[Move]:
        '''Generate the list of moves used to reach the current position
        @return List of moves from start_board to curPos'''
        raise NotImplementedError()
    def board_already_seen(self, board: Board) -> bool:
        raise NotImplementedError()
    def get_comment(self) -> Optional[str]:
        '''Return the comment at the current board.'''
        raise NotImplementedError()
    def set_comment(self, str):
        '''Overwrite comment at current board'''
        raise NotImplementedError()
    @abstractmethod
    def start_pos(self) -> Board:
        raise NotImplementedError()
    
    # Tags
    
    def clear_tags(self):
        self.tags = dict()
        self.tag_keys = []
    def list_tags(self):
        return self.tag_keys
    def get_tag(self, key):
        return self.tags.get(key.lower())
    def set_tag(self, key, value):
        if value == None:
            # Delete tag
            if key.lower() in self.tags:
                del self.tags[key.lower()]
                self.tag_keys = [k for k in self.tag_keys if k.lower() != key.lower()]
        else:
            # Create/update tag
            if key.lower() not in self.tags:
                self.tag_keys.append(key)
            self.tags[key.lower()] = value

class LinearGame(Game):
    '''Game implementation that uses a list as basic structure. No variants possible'''
    
    def __init__(self):
        super().__init__()
        self.moves = []
        self.start_board = None
        self._current_board = None
        self.moves_done = 0
        self.comment = []
    
    def __copy__(self):
        '''Create a shallow copy, ideal for getting an iterator'''
        copy = LinearGame()
        copy.moves = self.moves
        copy.start_board = self.start_board
        copy._current_board = self.start_board.copy()
        copy.moves_done = 0
        copy.comment = self.comment
        return copy
    
    def __deepcopy__(self):
        '''Create a deep copy, ideal for copying the game and all moves - changes will not affect the original'''
        copy = LinearGame()
        copy.moves = self.moves[:]
        copy.start_board = self.start_board.copy()
        copy._current_board = self.start_board.copy()
        copy.moves_done = 0
        copy.comment = self.comment[:]
        return copy
    
    def iter(self):
        return self.__copy__()
    def clone(self):
        return self.__deepcopy__()
        
    def restart_from(self, board: Board):
        '''Clear all data, and set up a new start position.'''
        self.moves = []
        if isinstance(board, Board):
            self.start_board = board.copy()
        else:
            # assert isinstance(board, BoardGrid):
            # may trow an error
            self.start_board = Board(board)
        self._current_board = self.start_board.copy()
        self.moves_done = 0
        self.comment = []
    
    def do_move(self, move: Move):
        '''
          Do a move on the game board. This function also maintains the move tree,
          and automatically expands the tree if a new variant is started in the
          middle of a game. The tree can hold several variants.
          @param move  move to do at the current board
          @pre move  must be a valid normalised move
        '''
        self._current_board.do_move(move)
        if (len(self.moves) == self.moves_done
        or move != self.moves[self.moves_done]):
            # Replace end of moves
            self.moves[self.moves_done:] = [move]
        self.moves_done += 1
    
    def redo_move(self, move: Move = None) -> int:
        '''
          Redo a move on the game board. Only moves that are already present in
          the move tree are valid.
          @param move  move to do at the current board
          @pre move  must be a valid normalised move in the game tree
          @return 0 on success, 
                  1 if the move was not found in the move tree at the current node.
                  2 if the move was invalid

        '''
        if len(self.moves) <= self.moves_done:
            return 1
        if move is None:
            move = self.moves[self.moves_done]
        if self.moves[self.moves_done] != move:
            return 1
        
        try:
            self._current_board.do_move(move)
        except HaliotisException:
            return 2
        
        self.moves_done += 1
        return 0
    
    def undo_move(self):
        if self.moves_done <= 0:
            return
        
        # Redo all moves up to prev move
        self._current_board = self.start_board.copy()
        self.moves_done -= 1
        for i in range(self.moves_done):
            self._current_board.do_move(self.moves[i])
    
    def undo_all_moves(self):
        self._current_board = self.start_board.copy()
        self.moves_done = 0
    
    def more_moves_to_undo(self) -> bool:
        '''Are we at the root'''
        return self.moves_done > 0
    
    def more_moves_to_redo(self) -> bool:
        '''From current position, have we reached the end of the primary variation?'''
        return self.moves_done < len(self.moves)
    
    def prev_move(self) -> Optional[Move]:
        if self.more_moves_to_undo():
            return self.moves[self.moves_done-1]
        return None
    
    def next_move(self) -> Optional[Move]:
        if self.more_moves_to_redo():
            return self.moves[self.moves_done]
        return None
    
    def alternate_moves(self) -> List[Move]:
        '''
          Return all registered moves at the current board.
            @note To do one of these moves, use DoMove.
        '''
        return self.moves[self.moves_done : self.moves_done+1]
    
    def current_board(self) -> Board:
        return self._current_board
    
    def current_board_number(self) -> int:
        '''Compute number of moves used to reach the current position from start_board.'''
        return self.moves_done
    
    def current_moves(self) -> List[Move]:
        '''Generate the list of moves used to reach the current position
        @return List of moves from start_board to curPos'''
        return self.moves[0: self.moves_done]
    
    def board_already_seen(self, board: Board) -> bool:
        raise NotImplementedError()
    
    def get_comment(self) -> Optional[str]:
        '''Return the comment at the current board.'''
        if len(self.comment) <= self.moves_done:
            return None
        return self.comment[self.moves_done]
        
    def set_comment(self, str):
        '''Overwrite comment at current board'''
        if len(self.comment) <= self.moves_done:
            items_to_add = 1 + self.moves_done - len(self.comment)
            self.comment.extend(items_to_add * [None])
        self.comment[self.moves_done] = str
    
    def start_pos(self) -> Board:
        return self.start_board

#
#  Tree game
#

class TreeGameNode:
    '''One node in the game tree'''
    def __init__(self, move: Move):
        self.move = move
        self.comment = None
        self.prev = None
        self.next = None
        self.alt = None

class TreeGame(Game):
    '''Game implementation that uses a tree as basic structure.'''
    
    def __init__(self):
        self._root = None
        self.start_board = None
        self._current_board = None
        self._current_node = None
    
    def __copy__(self):
        '''Create a shallow copy, ideal for getting an iterator'''
        copy = TreeGame()
        copy._root = self._root
        copy.start_board = self.start_board
        copy._current_board = self._current_board
        copy._current_node = self._current_node
        return copy
    
    def __deepcopy__(self):
        '''Create a deep copy, ideal for copying the game and all moves - changes will not affect the original'''
        copy = TreeGame()
        copy.restart_from(self.start_board)
        raise NotImplementedError()
        # TODO: Copy nodes inside tree - that sounds like recursion
        # make sure that copy._current_node corresponds to that of self
        return copy
    
    def iter(self):
        return self.__copy__()
    def clone(self):
        return self.__deepcopy__()
    
    def restart_from(self, board: Board):
        '''Clear all data, and set up a new start position.'''
        self._root = None
        if isinstance(board, Board):
            self.start_board = board.copy()
        else: # BoardGrid = List[List[int]]
            self.start_board = Board(board)
        self._current_board = self.start_board.copy()
        assert isinstance(self._current_board, Board)
        self._current_node = None
    
    def do_move(self, move: Move):
        '''
          Do a move on the game board. This function also maintains the move tree,
          and automatically expands the tree if a new variant is started in the
          middle of a game. The tree can hold several variants.
          @param move  move to do at the current board
          @pre move  must be a valid normalised move
        '''
        self._current_board.do_move(move)
        # Search for node with move
        p = None
        n = self._current_node
        while n is not None:
            if n.move == move:
                # Found it
                self._current_node = n
                return 0
            p = n
            n = n.alt
        # Add new node
        n = TreeGameNode(move)
        self._current_node = n
        if p is None:
            # First node in tree
            self._root = n
        else:
            assert p.alt is None
            p.alt = n
            n.prev = p.prev
        return 0
    
    def redo_move(self, move: Move = None) -> int:
        '''
          Redo a move on the game board. Only moves that are already present in
          the move tree are valid.
          @param move  move to do at the current board
          @pre move  must be a valid normalised move in the game tree
          @return 0 on success, 
                  1 if the move was not found in the move tree at the current node.
                  2 if the move was invalid

        '''
        if move is None:
            return self.redo_move_mainline()
        if move not in self.alternate_moves():
            return 1
        
        try:
            self._current_board.do_move(move)
        except HaliotisException:
            return 2
        
        return 0
    
    def moves_done(self):
        '''Return a list of all moves played so far'''
        moves = []
        n = self._current_node
        while n is not None:
            moves.append(n.move)
            n = n.prev
        moves.reverse()
        return moves
    
    def _compute_current_board(self):
        '''Replay all moves up to current node, and recompute _current_board'''
        b = self.start_board.copy()
        for m in self.moves_done():
            b.do_move(m)
        self._current_board = b
    
    def undo_move(self):
        '''Back up current move'''
        if self._current_node is None:
            return False
        self._current_node = self._current_node.prev
        self._compute_current_board()
        return True
    
    def undo_all_moves(self):
        '''Back up all moves'''
        self._current_node = None
        self._compute_current_board()
    
    def redo_move_mainline(self):
        '''Redo main line move
          Will redo the main line of moves.
        @see Game::AlternateMoves for instruction on how to redo variants.
        '''
        if self._current_node is None:
            n = self._root
        else:
            n = self._current_node.next
        
        if n is None:
            return False
        
        move = n.move 
        self._current_board.do_move(move)
        self._current_node = n
        
        return True

    def more_moves_to_undo(self) -> bool:
        '''Are we at the root'''
        return self._current_node is not None
    
    def more_moves_to_redo(self) -> bool:
        '''From current position, have we reached the end of the primary variation?'''
        if self._current_node is None:
            n = self._root
        else:
            n = self._current_node.next
        
        return n is not None
    
    def prev_move(self) -> Optional[Move]:
        '''Return the last played move'''
        return None if self._current_node is None else self._current_node.move
    
    def next_move(self) -> Optional[Move]:
        ''' Return the next move of the main line'''
        next_node = self._root if self._current_node is None else self._current_node.next
        return None if next_node is None else next_node.move
    
    def alternate_moves(self) -> List[Move]:
        '''
          Return all registered moves at the current board.
            @note To do one of these moves, use DoMove.
        '''
        moves = []
        if self._current_node is None:
            n = self._root
        else:
            n = self._current_node.next
        while n is not None:
            moves.append(n.move)
            n = n.alt
        return moves
    
    def current_board(self) -> Board:
        if not isinstance(self._current_board, Board):
            raise HaliotisException('Game board not initialized')
        return self._current_board
    
    def current_board_number(self) -> int:
        '''Compute number of moves used to reach the current position from start_board.'''
        return len(self.current_moves())
    
    def current_moves(self) -> List[Move]:
        '''Generate the list of moves used to reach the current position
        @return List of moves from start_board to curPos'''
        return self.moves_done()
    
    def board_already_seen(self, board: Board) -> bool:
        raise NotImplementedError()
    
    def get_comment(self) -> str:
        '''Return the comment at the current board.'''
        if self._current_node is None:
            raise NotImplementedError()
        return self._current_node.comment
        
    def set_comment(self, str):
        '''Overwrite comment at current board'''
        if self._current_node is None:
            raise NotImplementedError()
        self._current_node.comment = str
    
    def start_pos(self) -> Board:
        return self.start_board
