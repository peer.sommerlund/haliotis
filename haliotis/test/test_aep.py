import unittest
import random

from haliotis.aep import *
from haliotis.persistence import as_ab_move
from haliotis import BelgianDaisy, LinearGame


def random_move(board):
    return random.choice(list(board.moves()))


class MockEngine(Engine):
    # Define abstract methods
    def set_game(self, game):
        self.game = game
    def get_name(self):
        raise NotImplementedError()
    def get_author(self):
        raise NotImplementedError()
    def get_move(self, **search_param):
        raise NotImplementedError()
    def stop_search(self):
        raise NotImplementedError()

class CmdPositionTest(unittest.TestCase):
    '''Test the position command'''

    def test_invalid_commands(self):
        eng = MockEngine()
        aep = EngineWrapper(eng)
        with self.assertRaises(ValueError):
            aep.handle_command('position 234234234234234234 moves 32423423423')
        with self.assertRaises(ValueError):
            aep.handle_command('position abp 234234234234234234 moves 32423423423')
        with self.assertRaises(ValueError):
            aep.handle_command('position abp 2010231212012012012021021012021021021021021021234234234234234234')
        with self.assertRaises(ValueError):
            aep.handle_command('position abp 2201122211102200110000000000000000000000000011022011122211022100 a1d4')
    
    def test_simple_valid_commands(self):
        eng = MockEngine()
        aep = EngineWrapper(eng)
        self.assertIsNone(aep.handle_command('position abp       22.11222111.22..1.....1...........2.........11..2.11122211.22100'.replace('.','0') ))
        self.assertIsNone(aep.handle_command('position abp 2201122211102200110000000000000000000000000011022011122211022100 moves a1d4'))
    
    def test_random_game(self):
        eng = MockEngine()
        aep = EngineWrapper(eng)
        g = LinearGame()
        b1 = Board(BelgianDaisy)
        g.restart_from(b1)
        M = 40
        for _ in range(M):
            m = random_move(g.current_board())
            g.do_move(m)
        b2 = g.current_board().copy()

        # Test with no moves
        board_str = repr(b1).replace('\n','').replace(' ','').replace('.','0')
        self.assertIsNone(aep.handle_command(f'position abp {board_str}'))
        # verif start position
        self.assertEqual(eng.game.start_board, b1)
        # verify position unchanged after no moves
        self.assertEqual(eng.game.current_board(), b1)

        # Test with a list of moves
        board_str = repr(b1).replace('\n','').replace(' ','').replace('.','0')
        move_str = ' '.join([as_ab_move(None, m) for m in g.current_moves()])
        aep.handle_command(f'position abp {board_str} moves {move_str}')
        # verif start position
        self.assertEqual(eng.game.start_board, b1)
        # verify position after all moves
        self.assertEqual(eng.game.current_board(), b2)

