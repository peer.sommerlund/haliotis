import unittest

from haliotis import Pos, BelgianDaisy, Board
from haliotis.game import *
from haliotis.persistence import *

class AgFileFormatTest(unittest.TestCase):
    def test_as_ab_pos(self):
        self.assertEqual('i1', as_ab_pos(Pos(0,0)))
        self.assertEqual('a1', as_ab_pos(Pos(0,8)))
        self.assertEqual('a9', as_ab_pos(Pos(8,8)))
        self.assertEqual('i9', as_ab_pos(Pos(8,0)))

    def test_parse_ab_pos(self):
        self.assertEqual(parse_ab_pos('i','1'), Pos(0,0))
        self.assertEqual(parse_ab_pos('a','1'), Pos(0,8))
        self.assertEqual(parse_ab_pos('a','9'), Pos(8,8))
        self.assertEqual(parse_ab_pos('i','9'), Pos(8,0))
    
    def test_convert_ab_move(self):
        '''Test global function that parses string into move, but needs board to remove ambuiguity.'''
        # Test valid moves
        b = Board(AllMoves)
        #print(b)
        for m in b.moves():
            #print(m)
            m2 = convert_ab_move(b, m.from_first, m.to_last)
            self.assertEqual(m, m2)
            
        # Test invalid moves
        def s2p(s):
            return Pos(eval(s[0]), eval(s[1]))
        invalid_moves = ['2252', '8242', '7242', '1331', '6242', '5242','7251','7261','6251']
        for ms in invalid_moves:
            ff = s2p(ms[0:2])
            tl = s2p(ms[2:4])
            m = convert_ab_move(b, ff, tl)
            self.assertEqual(m, None)
    
        b = Board(BelgianDaisy)
        # Invalid head
        ff = parse_ab_pos('a','9')
        tl = parse_ab_pos('b','2')
        m = Move(ff,tl)
        self.assertRaises(HaliotisException, convert_ab_move,b,ff,tl)
        # Invalid tail
        ff = parse_ab_pos('a','1')
        tl = parse_ab_pos('i','2')
        m = Move(ff,tl)
        self.assertRaises(HaliotisException, convert_ab_move,b,ff,tl)
        # inline head-only of 3
        ff = parse_ab_pos('a','1')
        tl = parse_ab_pos('b','2')
        m = Move(ff,tl)
        print(b)
        print(as_ab_move(b,m))
        self.assertNotEqual(convert_ab_move(b,ff,tl), m)
        # inline 3 pieces
        ff = parse_ab_pos('a','1')
        tl = parse_ab_pos('d','4')
        m = Move(ff,tl)
        print(b)
        print(as_ab_move(b,m))
        self.assertEqual(convert_ab_move(b,ff,tl), m)
        # broadside
        ff = parse_ab_pos('a','3')
        tl = parse_ab_pos('a','3')
        m = Move(ff,tl)
        print(b)
        print(as_ab_move(m))
        self.assertEqual(convert_ab_move(b,ff,tl), m)

    def test_agf_tokenizer(self):
        def check_toktyp(str, tt):
            p = AGF_Tokenizer(str)
            p.next_tok()
            self.assertEqual(p.toktyp, tt)
            return p
        # Positive test
        check_toktyp('23.', Tok.MovNum)
        check_toktyp('5. -', Tok.MovNum)
        check_toktyp('1-0', Tok.EOG)
        check_toktyp('1/2-1/2', Tok.EOG)
        check_toktyp('0-1', Tok.EOG)
        check_toktyp('*', Tok.EOG)
        p = check_toktyp('{comment}', Tok.Comment)
        self.assertEqual(p.get_comment(), 'comment')
        p = check_toktyp(r'{com\{m\}ent}', Tok.Comment)
        self.assertEqual(p.get_comment(), 'com{m}ent')
        check_toktyp('(', Tok.VariantBegin)
        check_toktyp(')', Tok.VariantEnd)
        check_toktyp('a2b3', Tok.Move)
        check_toktyp('i6h6', Tok.Move)

    def test_agf_read_write_simple_LinearGame(self):
        '''Minimal AGF implementation - serialize and deserialize a game with no comments and no variants'''
        # Provide game header
        g = LinearGame()
        g.restart_from(Board(BelgianDaisy))
        g.set_tag('Black','Random player')
        g.set_tag('White','Monkey')
        # Generate moves
        ms = []
        import random
        for i in range(10):
            print(i)
            m = random.choice(list(g.current_board().moves()))
            ms.append(m)
            m_str = as_ab_move(g.current_board(), m)
            g.set_comment(f'next move is {m_str}')
            g.do_move(m)
        # Write to buffer
        import io
        fo = io.StringIO()
        agf_write(g, fo)
        print('fo=#{}#'.format(fo.getvalue()))
        # Read from buffer
        fi = io.StringIO(fo.getvalue())
        # Test that roundtrip went well
        g2 = LinearGame()
        print('about to read what we have just written')
        agf_read(g2, fi)
        print('read {} moves: {}'.format(len(g2.moves), g2.moves))
        self.assertEqual(g.current_board_number(), g2.current_board_number())
        g.undo_all_moves()
        g2.undo_all_moves() # TODO should this happen automatically?
        n = 0
        while g.more_moves_to_redo():
            print('  move {}: {}'.format(n+1, g2.next_move()))
            self.assertEqual(ms[n], g2.next_move())
            self.assertEqual(g.next_move(), g2.next_move())
            n += 1
            g.redo_move()
            g2.redo_move()
        self.assertEqual(g2.get_tag('Result'), '*')
    def test_agf_write_LinearGame(self):
        '''AGF implementation - serialize a game'''
        g = LinearGame()
        g.restart_from(Board(BelgianDaisy))
        g.set_tag('Black','Random player')
        g.set_tag('White','Monkey')
        import random
        for i in range(10):
            print(i)
            m = random.choice(list(g.current_board().moves()))
            m_str = as_ab_move(g.current_board(), m)
            g.set_comment(f'next move is {m_str}')
            g.do_move(m)
        import io
        fo = io.StringIO()
        agf_write(g, fo)
        print('#{}#'.format(fo.getvalue()))
        # TODO test that serialization is correct
    
    def test_agf_read_LinearGame(self):
        '''AGF implementation - deserialize a game'''
        filename = 'ace-lin.agf'
        g = LinearGame()
        import os
        try:
            with open(os.curdir + '/haliotis/test/' +filename) as fi:
                agf_read(g,fi)
        except FileNotFoundError:
            print(f'**** {filename} not found ****')
            raise
        except SyntaxError:
            print(f'**** {filename} not parseable ****')
            raise
        self.assertEqual(g.get_tag('Black'),'Random player')
        self.assertEqual(g.get_tag('White'),'Monkey')
        import random
        for i in range(10):
            m = random.choice(list(g.current_board().moves()))
            m_str = as_ab_move(g.current_board(), m)
            g.set_comment(f'next move is {m_str}')
            g.do_move(m)
        import io
        fo = io.StringIO()
        agf_write(g, fo)
        print('#{}#'.format(fo.getvalue()))
        # TODO test that serialization is correct
    
    def load_file(self, filename):
        g = TreeGame()
        import os
        try:
            with open(os.curdir + '/haliotis/test/' +filename) as fi:
                agf_read(g,fi)
        except FileNotFoundError:
            print(f'**** {filename} not found ****')
            raise
        return g
    def test_load_agf(self):
        '''AGF implementation - load example file from specs'''
        g = self.load_file('aceboard-2005.agf')
        # Standard roster tag
        self.assertEqual(g.get_tag('Site'),'NetAbalone')
        self.assertEqual(g.get_tag('Date'),'2005.03.19')
        self.assertEqual(g.get_tag('Black'),'Eobllor')
        self.assertEqual(g.get_tag('White'),'Gohatto')
        self.assertEqual(g.get_tag('Result'),'0-1')
        self.assertEqual(g.get_tag('Annotator'),'Alex Borello')

        # Tag case insensitivity
        self.assertEqual(g.get_tag('black'),'Eobllor')
        self.assertEqual(g.get_tag('bLaCk'),'Eobllor')
        
        # Main line
        def move(str):
            m = parse_move(g.current_board(), str)
            assert m is not None
        self.assertIsNone(g.prev_move())
        self.assertFalse(g.more_moves_to_undo())
        self.assertFalse(g.undo_move())
        self.assertTrue(g.redo_move())
        self.assertTrue(g.more_moves_to_undo())
        self.assertEqual(g.prev_move(), move('a1d4'))
        assert g.redo_move()
        self.assertEqual(g.prev_move(), move('i5h5'))
        assert g.redo_move()
        # Comments
        self.assertEqual(g.prev_move(), move('a2b3'))
        self.assertIsNone(g.get_comment())
        assert g.redo_move()
        self.assertEqual(g.prev_move(), move('i6h6'))
        self.assertEqual(g.get_comment(),'The classical beginning')
        self.assertTrue(g.undo_move())
        self.assertIsNone(g.get_comment())
        g.set_comment('Testing 1, 2, 3')
        self.assertEqual(g.get_comment(),'Testing 1, 2, 3')
        assert g.redo_move()
        self.assertEqual(g.get_comment(),'The classical beginning')
        
