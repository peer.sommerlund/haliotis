import unittest
from haliotis.board2D import *

class DirTest(unittest.TestCase):
    def test_opposite(self):
        ds = [Dir(i%6) for i in range(9)]
        for i in range(6):
            d1 = ds[i]
            d2 = ds[i+3]
            self.assertEqual(d1.opposite(), d2)

class PosTest(unittest.TestCase):
    def test_step(self):
        a = Pos(4,4)
        for d in range(6):
            # Sideeffects!
            p = Pos(a)
            self.assertEqual(p,a)
            p.step(Dir(d))
            self.assertNotEqual(p,a)
    def test_neighbour(self):
        a = Pos(4,4)
        for d in range(6):
            # No sideeffects
            p = Pos(a)
            x = p.neighbour(Dir(d))
            self.assertEqual(p,a)

class MoveTest(unittest.TestCase):
    def test_is_valid(self):
        m = Move()
        self.assertFalse(m.is_valid())
        # Invalid moveDir
        m.moveDir = Dir.Invalid
        self.assertFalse(m.is_valid())
        # Invalid tailDir
        m.moveDir = Dir.Left
        m.tailDir = Dir.Invalid
        self.assertFalse(m.is_valid())
        # Invalid from_first
        m.tailDir = Dir.LeftDown
        m.head = Pos(-1,-1)
        self.assertFalse(m.is_valid())
        # Invalid to_first
        m.head = Pos(0,4) #+movedir=Pos(-1,4)
        self.assertFalse(m.is_valid())
        # Too low tail count
        m.tailCount = -1
        self.assertFalse(m.is_valid())
        # Too high tail count
        m.head = Pos(5,3)
        m.tailCount = 4
        self.assertFalse(m.is_valid())
        # Invalid from_last
        m.tailCount = 3
        m.head = Pos(1,7) # + 2*tailDir = Pos(-1,9)
        self.assertFalse(m.is_valid())
        # Invalid to_last
        m.head = Pos(2,6) # + 2*tailDir = Pos(0,8) + moveDir = Pos(-1,8)
        self.assertFalse(m.is_valid())
        # push too much
        m.pushCount = m.tailCount
        self.assertFalse(m.is_valid())
        # Everything ok
        m.head = Pos(4,4)
        m.pushCount = 0
        self.assertTrue(m.is_valid())
    
AllMoves = [
  [0, 0, 0, 0,   1, 0, 0, 0, 0],
   [0, 0, 0,   2, 2, 0, 1, 0, 0],
    [0, 0,   1, 2, 2, 1, 1, 1, 0],
     [0,   1, 2, 2, 0, 0, 0, 0, 0],
      [  0, 0, 2, 0, 0, 0, 0, 0, 0],
       [  2, 1, 1, 1, 2, 0, 0, 0,   0],
        [  2, 2, 1, 1, 1, 2, 2,   0, 0],
         [  1, 1, 2, 0, 2, 2,   0, 0, 0],
          [  0, 0, 0, 0, 0,   0, 0, 0, 0]
];
class BoardTest(unittest.TestCase):
    def test_copy(self):
        '''Is Board.copy() really a copy'''
        a = Board(TheWall)
        b = a.copy()
        self.assertEqual(a,b)
        a.do_move(list(a.moves())[0])
        self.assertNotEqual(a,b)
        self.assertNotEqual(a.player_to_move, b.player_to_move)
        a.do_move(list(a.moves())[0])
        self.assertEqual(a.player_to_move, b.player_to_move)
    
    def test_moves(self):
        '''Can Board.moves() generate all moves'''
        def count_moves_from(g):
            b = Board(g)
            #print(b)
            move_count = 0
            for m in b.moves():
                #print(m)
                move_count += 1
            return move_count
        self.assertEqual(count_moves_from(BelgianDaisy), 52)
        self.assertEqual(count_moves_from(AllMoves), 60)
        
        # Test that generated moves are valid, by doing a random game
        import random
        b = Board(BelgianDaisy)
        move_count = 0
        for _ in range(30):
            move_count += 1
            ms = list(b.moves())
            m = ms[random.randrange(len(ms))]
            b.do_move(m) # will raise exception if invalid
        
    @unittest.skip('Not implemented')
    def test_do_move(self):
        # inline 1-0
        # inline 1-1
        # inline 2-0
        # inline 2-1
        # inline 2-2
        # inline 3-0
        # inline 3-1
        # inline 3-2
        # inline 3-3
        # inline 3-1-1
        # inline 3-2-1
        # inline 1-*
        # inline 2-*
        # inline 3-*
        # broadside 2L
        # broadside 3L
        # broadside 3L-block @1
        # broadside 3L-block @2
        # broadside 3L-block @3
        # broadside 3R-block @1
        # broadside 3R-block @2
        # broadside 3R-block @3
        raise NotImplementedError('TODO')
        
    def test_deserialize(self):
        '''Test conversion of string into Board'''
        s1 = "    2 2 . . .\n   2 2 . 1 1 .\n  . 2 2 1 1 1 .\n . . 2 . 1 1 . .\n. . . . . . . . .\n . . . . . . 2 .\n  . 1 1 . 2 2 .\n   1 1 1 2 2 2\n    1 1 . . 2\n"
        with self.assertRaises(ValueError):
            b = Board.parse(s1)
        
        s2 = s1 + "1 0 0"
        b = Board.parse(s2)
        a2 =[
              [0, 0, 0, 0,   2, 2, 0, 0, 0],
               [0, 0, 0,   2, 2, 0, 1, 1, 0],
                [0, 0,   0, 2, 2, 1, 1, 1, 0],
                 [0,   0, 0, 2, 0, 1, 1, 0, 0],
                  [  0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [  0, 0, 0, 0, 0, 0, 2, 0,   0],
                    [  0, 1, 1, 0, 2, 2, 0,   0, 0],
                     [  1, 1, 1, 2, 2, 2,   0, 0, 0],
                      [  1, 1, 0, 0, 2,   0, 0, 0, 0]
        ]
        b2 = Board(a2)
        
        # Verify side to move is parsed
        
        s3 = s1 + "2 0 0"
        b3 = b2.copy()
        b3.player_to_move = 2
        
        self.assertEqual(b,b2)
        self.assertNotEqual(b,b3)
        
        b = Board.parse(s3)
        self.assertNotEqual(b,b2)
        self.assertEqual(b,b3)
