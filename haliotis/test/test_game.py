import unittest
import random

from haliotis.game import *
from haliotis import BelgianDaisy

def random_move(board):
    ms = list(board.moves())
    return random.choice(ms)

class LinearGameTest(unittest.TestCase):
    def test_do_undo(self):
        g = LinearGame()
        b1 = Board(BelgianDaisy)
        g.restart_from(b1)
        M = 40
        for _ in range(M):
            m = random_move(g.current_board())
            g.do_move(m)
        b2 = g.current_board().copy()
        self.assertNotEqual(b1, b2)
        for _ in range(M):
            g.undo_move()
        self.assertEqual(b1, g.current_board())
