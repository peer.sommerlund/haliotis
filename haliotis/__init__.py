"""
    haliotis
    --------
    
    A library for playing the board game Abalone
"""

__version__ = '0.2-dev'

from .exception import HaliotisException



from .board2D import Dir

from .board2D import Pos, \
    all_pos

from .board2D import Move

from .board2D import Board, \
    BoardGrid, \
    GermanDaisy, BelgianDaisy, SwissDaisy, DutchDaisy, TheWall



from .game import Game, LinearGame



from .aep import Engine, play



from .persistence import do_FFTL, as_ab_move, \
    parse_ab_pos, \
    parse_move, \
    as_ab_board, \
    move_list, \
    convert_ab_move



from .time import TournamentClock
