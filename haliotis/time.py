#
#   Haliotis, a library for Abalone playing programs.
#
#   :license: LGPL, see LICENSE for details.
#   :copyright: 2019 Peer Sommerlund
#
"""
    This module defines various functions to measure time
"""

from time import time

#
#  Clock
#

class TournamentClock:
    '''Multi clock for tournaments'''
    
    def __init__(self):
        self._remain = [0,0]
        self._increment = [0,0]
        self._current_clock = 0
        self._started_at = None
    
    def __str__(self):
        rs = self.remaining()
        return '({})'.format(','.join(map(str,rs)))
    
    def current_clock(self):
        '''Return remaining time for current clock'''
        t = self._remain[self._current_clock]
        if self._started_at:
            t += time() - self._started_at
        return t
    
    def remaining(self):
        '''Return a list of remaining time for each clock'''
        r = self._remain[:]
        r[self._current_clock] = self.current_clock()
        return r
    
    def increment(self):
        '''Return a list of increment time for each clock'''
        return self._increment[:]
    
    def set_remaining(self, *rem_sec):
        '''Adjust remaining time for all clocks'''
        self._remain = list(rem_sec)
    
    def set_increment(self, *inc_sec):
        '''Adjust how much time is added to clock every time a move is done.
        The increment is specified per clock. It is added when changing clock.'''
        self._increment = inc_sec[:]
    
    def start(self):
        '''Continue current clock.'''
        if self._started_at:
            return # Already started
        self._started_at = time()
    
    def stop(self):
        '''Pause current clock.'''
        if self._started_at:
            self._remain[self._current_clock] -= time() - self._started_at
        self._started_at = None

    def change_clock(self, i: int):
        '''Change the clock that is running. This will add the increment
        to the stopped clock.'''
        assert 0 <= i and i < len(self._remain)
        if i == self._current_clock:
            return
        self.stop()
        self._remain[self._current_clock] += self._increment[self._current_clock]
        self._current_clock = i
        self.start()
