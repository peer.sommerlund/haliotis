#
#   Haliotis, a library for Abalone playing programs.
#
#   :license: LGPL, see LICENSE for details.
#   :copyright: 2019 Peer Sommerlund
#
"""
    This module defines various functions to persist moves
"""

from enum import IntEnum
import re
import logging

from . import Dir, Pos, Move, Board, BelgianDaisy, Game
from . import HaliotisException
from .board2D import InvalidMove

#
#  Logging
#

log = logging.getLogger('haliotis')

#
#  Persistence functions
#

def as_ab_pos(pos: Pos) -> str:
    '''Format haliotis.Pos as Abalone standard coordinate'''
    row = chr(ord('i') - pos.y)
    col = chr(ord('1') + pos.x)
    return row + col

def parse_ab_pos(row: str, col: str) -> Pos:
    '''Convert Abalone standard coordinate to haliotis.Pos
    @note row must be a lower-case letter'''
    assert 'a'<=row and row<='i' and '1'<=col and col<='9'
    bp = Pos()
    bp.y = ord('i') - ord(row)
    bp.x = ord(col) - ord('1')
    return bp

def as_ab_move(board: Board, move: Move):
    # TODO remove the board parameter
    '''Format haliotis.Move as FFTL move'''
    return as_ab_pos(move.from_first) + as_ab_pos(move.to_last)

def convert_ab_move(
  board: Board,
  from_first: Pos,
  to_last: Pos,
  expand: bool=False) -> Move:
    ''' convert a FFTL move to haliotis.Move
    @param board  Board before move is done
    @param from_first  First marble to move
    @param to_last  Destination of last player marble.
    Note that some opponent marbles may also be moved, in which case this
    is the first opponent marble to push.
    @param expand  If set, will allow push moves that only touches the first marble
    @return  the resulting move
    @raises InvalidMove if the move was invalid.
    '''
    # TODO: Consider how an head-push move should be handled
    # 1. as valid  - let do-move expand it
    # 2. as invalid, because to_last != Empty
    
    #
    # Convert move
    #
    move = Move()
    move.head = from_first
    dx = to_last.x - from_first.x
    dy = to_last.y - from_first.y
    
    if not (-4 <= dx and dx <= 4 and -4 <= dy and dy <= 4):
        raise InvalidMove("convert_ab_move {}{}: dx,dy={},{} -> invalid move".format(from_first,to_last,dx,dy))
    
    x = -1 # invalid move
    y = -2 # small broadside move
    # For small broadside moves, initialise tail with left-most direction
    tailDir = [
      [x,x,x,x,x,x,x,x,x        ],
      [ x,x,x,x,4,4,5,5,x       ],
      [  x,x,x,4,4,4,5,5,x      ],
      [   x,x,3,3,4,5,5,0,x     ],
      [    x,3,3,3,x,0,0,0,x    ],
      [     x,3,2,2,1,0,0,x,x   ],
      [      x,2,2,1,1,1,x,x,x  ],
      [       x,2,2,1,1,x,x,x,x ],
      [        x,x,x,x,x,x,x,x,x]
    ]
    tailCount = [
      [x,x,x,x,x,x,x,x,x        ],
      [ x,x,x,x,3,3,3,3,x       ],
      [  x,x,x,3,2,2,2,3,x      ],
      [   x,x,3,2,1,1,2,3,x     ],
      [    x,3,2,1,x,1,2,3,x    ],
      [     x,3,2,1,1,2,3,x,x   ],
      [      x,3,2,2,2,3,x,x,x  ],
      [       x,3,3,3,3,x,x,x,x ],
      [        x,x,x,x,x,x,x,x,x]
    ]
    moveDir = [
      [x,x,x,x,x,x,x,x,x        ],
      [ x,x,x,x,4,5,4,5,x       ],
      [  x,x,x,3,4,y,5,0,x      ],
      [   x,x,4,y,4,5,y,5,x     ],
      [    x,3,3,3,x,0,0,0,x    ],
      [     x,2,y,2,1,y,1,x,x   ],
      [      x,3,2,y,1,0,x,x,x  ],
      [       x,2,1,2,1,x,x,x,x ],
      [        x,x,x,x,x,x,x,x,x]
    ]

    td             = tailDir  [dy+4][dx+4]
    move.tailCount = tailCount[dy+4][dx+4]
    md             = moveDir  [dy+4][dx+4]
    opponent = 3 - board.turn()
    if md == -2:
        # short broadside move - must check board
        move.tailDir = Dir(td)
        p = move.head.neighbour(move.tailDir)
        if (board.at(p) != 0) :
            # Tail is to the left, movedir to the right
            move.moveDir = move.tailDir.right()
        else:
            # movedir is to the left, tail is to the right
            move.moveDir = move.tailDir
            move.tailDir = move.tailDir.right()
    elif td == -1 or md == -1:
        raise InvalidMove("convert_ab_move dx,dy={},{} -> tailDir={},moveDir={} -> invalid move".format(dx,dy,move.tailDir,move.moveDir))
    else:
        move.tailDir = Dir(td)
        move.moveDir = Dir(md)
        p = move.to_last
        while p.is_valid() and board.at(p) == board.turn() and expand:
            move.tailCount += 1
            p.step(move.moveDir)
        while p.is_valid() and board.at(p) == opponent:
            move.pushCount += 1
            p.step(move.moveDir)
        
    # This should never happen. tailCount is -1 only when tailDir is -1
    if move.tailCount == -1:
        raise InvalidMove("convert_ab_move dx,dy={},{} -> tailCount={} -> invalid move".format(dx,dy,move.tailCount))
    #
    # Validate move and raise exceptions on error
    #
    try:
        board.verify_move(move)
        msg = None
    except (AssertionError, HaliotisException) as e:
        msg = str(e)
    if msg:
        raise InvalidMove("convert_ab_move {}{}: {}".format(from_first, to_last, msg))

    # Validation passed
    return move


def do_FFTL(game: Game, fa, f1, ta, t1):
    '''If FFTL forms a valid move, perform it on the game and return True.
    Otherwise no change to the game and return False.
    '''
    if fa<'a' or 'i'<fa: return False # Parse error
    if f1<'1' or '9'<f1: return False # Parse error
    if ta<'a' or 'i'<ta: return False # Parse error
    if t1<'1' or '9'<t1: return False # Parse error
    from_first = parse_ab_pos(fa,f1)
    if not from_first.is_valid(): return False # Invalid move
    to_last = parse_ab_pos(ta,t1)
    if not to_last.is_valid(): return False # Invalid move
    move = convert_ab_move(game.current_board(),from_first,to_last)
    if move is None: return False
    try:
        game.do_move(move)
    except HaliotisException:
        return False # Invalid move
    return True

def parse_move(board, move_str, expand=False) -> Move:
    '''Parse an FFTL-move given the board context'''
    if len(move_str) not in [4,6]:
        raise HaliotisException(f'This str "{move_str}" has wrong length to be a move')
    from_first = parse_ab_pos(move_str[0],move_str[1])
    if not from_first.is_valid():
        raise HaliotisException('FromFirst is not a valid board position')
    to_last = parse_ab_pos(move_str[-2],move_str[-1])
    if not to_last.is_valid():
        raise HaliotisException('ToLast is not a valid board position')
    return convert_ab_move(board, from_first, to_last, expand=expand)

#
#  Board
#
def as_ab_board(board: Board) -> str:
    '''NOTE: This function is only used to generate content of the SetUp tag'''
    assert board.turn() == 1 and board.score(1) == 0 and board.score(2) == 0
    board_str = board.as_str_naked().replace(' ','').replace('\n',' ')
    return board_str[:-3].strip()

def parse_ab_board(board_str: str) -> Board:
    '''NOTE: This function is only used to parse the start position'''
    if len(board_str) < 64:
        side_to_move = '1'
        scores = '00' # assuming two players
        board_str += side_to_move + scores
    return Board.parse(board_str)


#
#  Game
#

# ABF protocol tools
def move_list(game: Game) -> str:
    '''Follow the line of moves executed since game start, and return it as a string'''
    return ' '.join(as_ab_move(_,m) for m in game.current_moves())

# Quoting function used inside AGF comments
def quote(str):
    '''Put backslashes in front of special chars'''
    return str.replace('\\',r'\\').replace('{',r'\{').replace('}',r'\}')
def unquote(str):
    '''Remove backslashes'''
    return str.replace(r'\}','}').replace(r'\{','{').replace(r'\\','\\')

# EBNF for agf file format
'''
  game == tag-section move-section end-of-game
  move-section = move-data { '(' move-section ')' } move-section
  move-data = [ move-nr ] /agfmove/ [ '{' comment-text '}' ]
  move-nr = num '.' [ '-' ]
  
  move-nr is always present at the start of a variant
  a variant starts after '(', or after the tag-section
  move-nr is always present before an odd ply move
  first move is ply 1, second move ply 2, third move ply 3, etc.
'''

def agf_write(game: Game, fo):
    '''Serialise Game to stream'''
    # Tags
    game.set_tag('SetUp', as_ab_board(game.start_board))
    for k in game.list_tags():
        v = game.get_tag(k)
        vv = v.replace('"','') # Delete delimiter chars
        fo.write(f'[{k} "{vv}"]\n')
    # Moves
    def write_move(root, begin_variant):
        '''begin_variant = True when starting a new variant'''
        
        # move number
        if root.current_board_number() % 2 == 0:
            # Even number of moves_done
            fo.write('{}. '.format(root.current_board_number() // 2 + 1))
        elif begin_variant:
            # Odd number of moves_done
            fo.write('{}. - '.format(root.current_board_number() // 2 + 1))
            
        # move tree
        main_line = True
        for m in root.alternate_moves():
            if main_line:
                main_move = m
                m_str = as_ab_move(root.current_board(), m)
                fo.write(f'{m_str}')

                # comment
                if root.get_comment():
                    esc_cmt = quote(root.get_comment())
                    fo.write(f' {{{esc_cmt}}}')
                
                main_line = False
            else:
                fo.write('(')
                i = root.iter() # Create variant iterator
                i.do_move(m)
                write_move_sequence(i, begin_variant=True)
                fo.write(')')
        # Go to next move in main line
        root.do_move(main_move)
    def write_move_sequence(root, begin_variant=False):
        '''Update iterator to end of route'''
        write_move(root, begin_variant)
        while root.more_moves_to_redo():
            fo.write(' ')
            write_move(root, False)

    # Write all variants from the beginning
    i = game.iter()
    i.undo_all_moves()
    write_move_sequence(i)
    
    # End-of-game
    result = game.get_tag('Result')
    if not result: result = '*'
    fo.write(f' {result}\n')

class SyntaxError(HaliotisException):
    def __init__(self, msg, buf, pos):
        '''Create a Syntax Error exception
        :msg: Error message
        :buf: Full buffer of text parsed
        :pos: Index into buffer where the error occurred
        '''
        self.message = msg
        self.input_buffer = buf
        self.pos = pos
    def __str__(self):
        p0 = self.pos
        while p0 > 0 and self.input_buffer[p0-1] != '\n' and self.pos - p0 < 8:
            p0 -= 1
        p9 = self.pos
        while p9 < len(self.input_buffer) and self.input_buffer[p9] != '\n' and p9 - self.pos < 8:
            p9 += 1
        return '{}\n at "{}"\n     {}^'.format(self.message, self.input_buffer[p0:p9], ' '*(self.pos - p0) )
class Tok(IntEnum):
    EOF = 1
    MovNum = 2
    Move = 3
    EOG = 4
    Comment = 5
    VariantBegin = 6
    VariantEnd = 7
class AGF_Tokenizer:
    def __init__(self, buf):
        self.buf = buf
        self.pos = 0
        self.tokmatch = None
        self.tok = None
        self.toktyp = None
    def tokens(self):
        self.next_tok()
        while self.tok is not None:
            yield self.tok
            self.next_tok()
    def next_tok(self):
        i = self.pos
        # Skip leading whitespace
        if i < len(self.buf):
            m = re.match(r'\s+', self.buf[i:])
            if m:
                i += m.end()
                self.pos = i
        if not (i < len(self.buf)):
            # all data consumed
            self.tokmatch = None
            self.tok = ''
            self.toktyp = Tok.EOF
            self.pos = len(self.buf)
            return self.tok
        pat = [
          (r'\d+\.( -)?', Tok.MovNum),
          (r'[01]-[01]', Tok.EOG),
          (r'1/2-1/2', Tok.EOG),
          (r'\*', Tok.EOG),
          (r'\{([^\\\}]|\\.)*\}', Tok.Comment),
          (r'\(', Tok.VariantBegin),
          (r'\)', Tok.VariantEnd),
          (r'([abcdefghi][123456789]){2,}', Tok.Move)]
        for p,tt in pat:
            cp = re.compile(p)
            m = cp.match(self.buf[i:])
            if m:
                self.tokmatch = m
                self.tok = m.group()
                self.toktyp = tt
                self.pos = i + len(self.tok)
                return self.tok
        log.error('Tokenizer.next_tok unable to match "{self.buf[i:]}"')
        raise SyntaxError('Unable to extract next token', self.buf, self.pos)

    def get_comment(self):
        esc_str = self.tokmatch.group(0)[1:-1]
        str = unquote(esc_str)
        return str

auto_tags = {'FileFormat', 'SetUp'}
def agf_read(game: Game, fi, strict=False):
    '''Deserialise Game from UTF-8 stream'''
    # Reset game
    game.clear_tags()
    game.restart_from(BelgianDaisy)
    # read tags
    line = fi.readline()
    while line.startswith('['):
        line = line.strip()
        # hack  assume [name "value"] - no extra blanks
        tag, value = line.split(' ', maxsplit=1)
        tag = tag[1:]
        value = value[1:-2]
        if tag == 'FileFormat':
            assert value == 'SimpleAbalone-1'
        else:
            game.set_tag(tag, value)
        line = fi.readline()
    while line.strip(" \n\r") == "":
        line = fi.readline()
    board_str = game.get_tag('SetUp')
    if board_str:
        start_board = parse_ab_board(board_str)
        game.restart_from(start_board)
    elif re.match(r'^( *[012.]){5,9} *', line.strip("\r\n")):
        # Old board-before-move syntax
        board_str = ""
        while re.match(r'^ *( [012.])*$', line):
            board_str += line
            line = fi.readline()
        # Side-to-move
        if re.match(r'^[12]$', line):
            board_str += line
            line = fi.readline()
        else:
            raise SyntaxError('Could not parse board-before-move side-to-move', line, 0)
        # Score
        m = re.match(r'^[12]-([0123456])$', line)
        if m is None:
            raise SyntaxError('Could not parse board-before-move score-1', line, 0)
        board_str += m.group(1)
        line = fi.readline()
        m = re.match(r'^[12]-([0123456])$', line)
        if m is None:
            raise SyntaxError('Could not parse board-before-move score-2', line, 0)
        board_str += m.group(1)
        line = fi.readline()
        board_str = board_str.replace('\n','').replace(' ','').replace('\r','').replace('.','0')
        start_board = parse_ab_board(board_str)
        game.restart_from(start_board)
    # read moves
    buf = ''
    fi_pos = 0
    while line is not None and not line.startswith('['):
        buf += line
        line = fi.readline()
        if fi_pos == fi.tell():
            break
        fi_pos = fi.tell()
    # TODO unread line
    reader = AGF_Tokenizer(buf)
    def raiseSyntaxError(msg):
        raise SyntaxError(msg, reader.buf, reader.pos - len(reader.tok))
    
    def parse_move_data(iter):
        '''move-data = [ move-nr ] /agfmove/ [ '{' comment-text '}' ]'''
        # Move number, optional
        if reader.toktyp == Tok.MovNum:
            reader.next_tok()
            # TODO verify number is correct
        
        # Extract move
        move_str = reader.tok
        board_pre_move = str(iter.current_board())
        try:
            move = parse_move(iter.current_board(), move_str, expand=not strict)
            iter.do_move(move)
        except (InvalidMove, AssertionError) as e:
            raiseSyntaxError('{}\nMove "{}" cannot be done on board\n{}'.format(e, move_str, board_pre_move))
        reader.next_tok()
        
        # Extract comment, optional
        if reader.toktyp == Tok.Comment:
            iter.set_comment(reader.get_comment())
            reader.next_tok()
        
    def parse_move_section(iter):
        '''move-section = move-data { '(' move-section ')' } move-section'''
        while reader.toktyp == Tok.MovNum or reader.toktyp == Tok.Move:
            base = iter.iter()
            parse_move_data(iter)
            while reader.toktyp == Tok.VariantBegin:
                reader.next_tok()
                parse_move_section(base.iter())
                if not reader.toktyp == Tok.VariantEnd:
                    raiseSyntaxError('Expected ")"')
                reader.next_tok()
        
    
    reader.next_tok() # First token
    while reader.toktyp == Tok.MovNum or reader.toktyp == Tok.Move:
        parse_move_section(game)
    # Check optional End-Of-Game marker
    if reader.toktyp == Tok.EOF:
        # Missing End-Of-Game marker - ignore
        pass
    elif reader.toktyp == Tok.EOG:
        eog_result = reader.tok
        tag_result = game.get_tag('Result')
        if tag_result is None:
            game.set_tag('Result', eog_result)
        elif tag_result != eog_result:
            raiseSyntaxError('tag-result and eog-result differ')
        reader.next_tok()
    else:
        raiseSyntaxError(f'Expected End-Of-Game marker but found {str(reader.toktyp)}')
    
