#
#   Haliotis, a library for Abalone playing programs.
#
#   :license: LGPL, see LICENSE for details.
#   :copyright: 2019 Peer Sommerlund
#
"""
    The AEP module implements the Abalone Engine Protocol.

    It has two user visible structures - an interface "Engine" and a function "play". The play function takes an Engine as argument. It contains the main function that communicates over stdio and calls your engine when needed. 
"""

from abc import ABC, abstractmethod
import logging
import queue
import sys
import threading

from . import Game, Board, Move, all_pos
from .persistence import as_ab_move
import haliotis

#
#  Logging
#

log = logging.getLogger('haliotis')


#
#  Engine interface
#

class Engine(ABC):
    '''
    Interface required by the aep.play() function.
    It declares methods for handling various AEP commands, in addition to providing callback methods that the engine can use when input should be processed.
    '''
    def __init__(self):
        self.input_handler = None
        self.output_handler = None
    
    def set_input_handler(self, callback):
        '''Set function to call during search. Default implementation stores it in self.input_handler, which is used by check_input().
        
        This function is used by Haliotis when your engine is launched via aep.play(Engine). Your engine should call the callback via check_input() to process AEP commands on stdin.
        
        Normaly, you would not implement this function, unless you want to do special processing at initialization time'''
        self.input_handler = callback
    def check_input(self):
        '''Let input handler process any pending AEP commands.
        You should call this function at regular interval.
        
        Some AEP commands will cause call back into the engine, so make sure your engine is ready for this, before you call this function.'''
        if self.input_handler:
            self.input_handler()
    def set_output_handler(self, callback):
        '''Set a write-style function used to send information. The function will be called with a string 
        
        self.output_handler(f'info a {a} b {b}')
        '''
        self.output_handler = callback

    @abstractmethod
    def set_game(self, game: Board):
        '''Set up board position and moves leading to it.
        Called on 'position' command.'''
        raise NotImplementedError()
    @abstractmethod
    def get_name(self) -> str:
        '''Get name of engine.
        Used for 'id name' reply to 'aep' initialization command.'''
        raise NotImplementedError()
    @abstractmethod
    def get_author(self) -> str:
        '''Get author of engine.
        Used for 'id author' reply to 'aep' initialization command.'''
        raise NotImplementedError()
    def set_debug(self, enable: bool):
        '''Control debug output.
        Not currently used'''
        raise NotImplementedError()
    @abstractmethod
    def get_move(self, **search_param) -> Move:
        '''Get move from engine (ask it do perform a search)
        Called on 'go' command. The returned move is sent via the 'bestmove' reply. While this function is running, you have to send 'info' replies as you see fit, simply by printing on stdout. In addition to 'info' replies, you also have to call check_input as it will check stdin for new commands. Right before you return the selected move, you should output a final 'info' reply with statistics for the entire search.'''
        raise NotImplementedError()
    @abstractmethod
    def stop_search(self):
        '''Tell engine to set a flag so it will stop searching.
        Called on 'stop' command.'''
        raise NotImplementedError()


#
#  Convert AEP commands over stdio into method calls
#

class stdin_reader(threading.Thread):
    # Inspired by https://github.com/flok99/feeks/blob/master/main.py
    '''A portable way of checking stdin without blocking: Let a thread block'''
    
    # Only one stdin in the program, so make this queue a class variable
    q = queue.Queue()
    
    def __init__(self):
        super().__init__(daemon=True)
        self._quit = False

    # Run start() to execute run() in the thread
    def run(self):
        log.debug('stdin thread started')
        while not self._quit:
            line = sys.stdin.readline()
            if line == "" and sys.stdin.closed():
                log.error('stdin thread aborted - stdin closed')
                self._quit = True
            self.q.put(line)
        log.debug('stdin thread terminating')

    def get(self, block=False):
        if self._quit:
            return None
        try:
            return self.q.get(block=block)
        except queue.Empty:
            return None
    
    def quit(self):
        self._quit = True

def str_move(move: Move, game: Game):
    '''Return a FFTL string representing a move before done'''
    return haliotis.as_ab_move(game.current_board(), move)

def read_move(str: str, game: Game) -> bool:
    '''Read one FFTL move and add it to game.'''
    # Format - AEP (from first, to last)
    if len(str) != 4: return False
    fa,f1,ta,t1 = str[0:4]
    return haliotis.do_FFTL(game, fa, f1, ta, t1)

def as_aep_position(game: Game) -> str:
    '''Return an AEP position command corresponding to the current board and moves'''
    board_str = game.current_board().as_str_naked().replace('\n','').replace(' ','').replace('.','0')
    dummy_board = None
    move_str = ' '.join([as_ab_move(dummy_board, m) for m in game.current_moves()])
    return f'position abp {board_str} moves {move_str}'


class EngineWrapper:
    '''Generic wrapper of an engine that allows it to communicate through the
  Abalone Engine Protocol (AEP). The engine must implement the :ref:Engine interface
  and at least once pr second call the InputHandler provided.

  The wrapper provides functions that can check stdin for commands and act
  on commands from the client.
    '''
    def __init__(self, engine: Engine):
        self.engine = engine
        self.game = haliotis.LinearGame()
        self.game.set_tag('MissingPosition',True)
        self.searching = False
        self.quit = False
        
        # Set up thread to read stdin
        self.reader_thread = stdin_reader()
        self.reader_thread.start()
        
        # Give engine a way to check input from stdin when it feels like it
        engine.set_input_handler(self.check_input)
        
        # Give engine a way to send data to stdout when it feels like it
        engine.set_output_handler(self.reply)

    def must_quit(self):
        return self.quit

    #
    # Command parsing tools
    #
    
    def set_command_line(self, command_line):
        '''Begin parsing a new command line'''
        self.command_line = command_line
        self.remaining = self.command_line
    
    def get_token(self):
        '''Return the next token, or '' if no more availables'''
        tok, _, self.remaining = self.remaining.partition(' ')
        return tok
    
    def more_tokens(self):
        '''True, if get_token can find more tokens'''
        return self.remaining != ''
    
    def get_tail(self):
        return self.remaining
  
    #
    # input / output tools
    #

    def check_input(self, block=False):
        '''Nonblocking read of stdin. If a command is read, it is also handled.
        @note Should be called from the Engine while searching to make it possible to interrupt search. '''
        while True:
            try:
                line = self.reader_thread.get(block)
            except KeyboardInterrupt:
                self.quit = True
                return
            if line is None: return
            self.handle_command(line.strip())

    def reply(self, str):
        '''Send the string to the GUI (through stdout), and add a
        newline '\n' to indicate end-of-reply.'''
        print(str)
    
    #
    # Command handling
    #

    def handle_command(self, original_command_line):
        log.debug("+HandleCommand : '{}'".format(original_command_line))
        self.set_command_line(original_command_line)
        cmd = self.get_token()
        if cmd == 'aep':         self.cmd_aep()
        elif cmd == 'debug':     self.cmd_debug()
        elif cmd == 'isready':   self.cmd_isready()
        elif cmd == 'setoption': self.cmd_setoption()
        elif cmd == 'position':  self.cmd_position()
        elif cmd == 'go':        self.cmd_go()
        elif cmd == 'stop':      self.cmd_stop()
        elif cmd == 'ponderhit': self.cmd_ponderhit()
        elif cmd == 'quit':      self.cmd_quit()
        else:
            log.error('Unknown command : "{}"'.format(original_command_line))
        log.debug("-HandleCommand : "+original_command_line)
    
    def cmd_aep(self):
        s = self.engine.get_name()
        if s:
            self.reply("id name "+s)
        s = self.engine.get_author()
        if s:
            self.reply("id author "+s)
        # TODO: Return options of engine
        self.reply("readyok")
    
    def cmd_debug(self):
        direction = self.get_token()
        if direction == "on":
            self.engine.set_debug(True)
        elif direction == "off":
            self.engine.set_debug(False)
        else:
            log.error("Error: Valid values for debug command are 'on' or 'off' : {}".format(self.command_line))
    
    def cmd_isready(self):
        '''
        This command is used to synchronize the engine with the GUI. When the GUI
        has sent a command or multiple commands that can take some time to
        complete, this command can be used to wait for the engine to be ready
        again or to ping the engine to find out if it is still alive.
        E.g. this should be sent after setting the path to the tablebases as this
        can take some time. This command is also required once before the engine
        is asked to do any search to wait for the engine to finish initializing.
        This command must always be answered with "readyok"
        '''
        self.reply("readyok")

    def cmd_setoption(self):
        '''This command is used to configure the engine'''
        log.warning("command 'setoption' not implemented.");
  
    def cmd_position(self):
        '''
        Define starting position and moves up to current position. Use this
        command to set up board including moves leading to it. After this
        command the GUI would typically send the "go" command.

        @note Any syntax or semantic error is a fatal error and will raise an
        exception.
        
        @example A sample command for the first few moves after Belgian Daisy
        position abp 00000 000000 0000000 00000000 000000000 00000000 00000000 000000 00000 moves a2a3 b7b6
        '''
        if self.searching:
            log.error("Cannot set up new position while searching")
            raise RuntimeError("Cannot set up new position while searching")
        
        t = self.get_token()
        if t != "abp":
            msg = f"command 'position' starts with '{t}' instead of 'abp'"
            log.critical(msg)
            raise ValueError(msg)
        
        # Parse start position
        board_str = ''
        while self.more_tokens():
            t = self.get_token()
            if t == "moves":
                break
            board_str += t # Concatenate startpos
        
        # Set up start position
        try:
            start_pos = Board.parse(board_str)
        except ValueError as e:
            msg = ("command 'position' subcommand 'abp' got invalid board string: "+str(e))
            log.critical(msg)
            raise ValueError(msg)
        self.game.restart_from(start_pos)
        self.game.set_tag('MissingPosition',None)
        
        # Parse moves
        while self.more_tokens():
            t = self.get_token()
            if not read_move(t, self.game):
                msg = ("command 'position' move #{} '{}' is invalid"+
                "\nBoard=\n{}").format(1+self.game.current_board_number(), t, self.game.current_board())
                log.critical(msg)
                raise ValueError(msg)
        
        # Transfer game to engine
        self.engine.set_game(self.game)
    
    def cmd_go(self):
        '''
        Begin searching the current position. This starts the engine and it may
        reply any time it wants and several times. It is recommended to reply at
        least every time a ply has been fully searched and every time the main line
        changes. The engine signals that it is done by sending a 'bestmove'
        reply. It is recommended to send search statistics as an 'info' reply
        right before 'bestmove'.

        @example A sample command for a 10 ply search, max time 9000 miliseconds
        go depth 10 movetime 9000
        '''
        # Begin searching

        if self.searching:
            log.error("Cannot start new search while searching")
            return
        
        if self.game.get_tag('MissingPosition'):
            log.error("No position to search. Remember to send 'position' command first.")
            return
        
        # Extract search parameters
        search_param = {}
        while self.more_tokens():
            # Parse subcommand
            cmd = self.get_token()
            if cmd == "searchmoves":
                search_param[cmd] = self.get_tail()
            elif cmd == "ponder":
                search_param[cmd] = True
                #m_engine->SetSearchParameter(cmd,"1");
            elif cmd == "time":
                search_param[cmd+'1'] = self.get_token()
                search_param[cmd+'2'] = self.get_token()
            elif cmd == "inc":
                search_param[cmd+'1'] = self.get_token()
                search_param[cmd+'2'] = self.get_token()
            elif cmd == "depth":
                search_param[cmd] = self.get_token()
            elif cmd == "mate":
                search_param[cmd] = self.get_token()
            elif cmd == "movetime":
                search_param[cmd] = self.get_token()
            elif cmd == "infinite":
                search_param['movetime'] = 'inf'
            elif cmd == "nodes":
                search_param[cmd] = self.get_token()
            else:
                log.warning("Unknown 'go' subcommand: '{}'".format(cmd))
        
        # Begin search current position
        self.searching = True
        log.debug('Launching engine search with parameters: {}'.format(search_param))
        bestmove = self.engine.get_move(**search_param)
        self.searching = False
        print('bestmove {}'.format(str_move(bestmove,self.game)))

    def cmd_stop(self):
        '''Stop searching and output bestmove
        
        Note: This command may be called from inside the search
        thus self.engine.get_move() should allow this callback
        Correct engine implementation of stop_search(), should
        exit search as fast as possible when returning from check_input()
        '''
        self.engine.stop_search()
  
    def cmd_ponderhit(self):
        log.warning("command 'ponderhit' not implemented.")

    def cmd_quit(self):
        self.quit = True
        self.cmd_stop()
        self.reader_thread.quit()


def play(engine: Engine):
    '''Drive engine through the abalone engine protocol (AEP).
    Runs an infinite loop that consumes AEP commands from stdin
    and sends responses back on stdout. The commands are sent to
    the provided engine.'''
    aep = EngineWrapper(engine)
    while not aep.must_quit():
        aep.check_input(block=True)
